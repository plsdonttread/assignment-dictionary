section .text
extern string_equals
extern string_length
global find_word

find_word:
	.loop:

		push rdi
		push rsi
		add rsi, 8
		call string_equals
		pop rsi
		pop rdi

		cmp rax, 1
		je .result
		mov rsi, [rsi]
		cmp rsi, 0
		je .err
		jmp .loop
	.result:
		mov rax, rsi
		jmp .end
	.err:
		mov rax, 0
	.end:
		ret

