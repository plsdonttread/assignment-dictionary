ASM = nasm
ASMFLAGS = -felf64
LD = ld
OBJ = main.o lib.o dict.o

all: main

main: $(OBJ)
	$(LD) -o $@ $^

%.o: %.asm
	$(ASM) $(ASMFLAGS) -o $@ $<

main.o: main.asm colon.inc words.inc
ifdef loop
	$(ASM) $(ASMFLAGS) -o $@ $< -D ENABLE_LOOP
else
	$(ASM) $(ASMFLAGS) -o $@ $<
endif

clean:
	rm main $(OBJ)

.PHONY: clean