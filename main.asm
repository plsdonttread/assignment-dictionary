%define BYTES_TO_READ 256
%define EXIT_OK 0 
%define EXIT_ERR 1

section .data
%include "words.inc"

err_too_long db "Key string too long", 10, 0
err_no_match db "No match found", 10, 0

section .text

extern exit
extern string_length
extern print_string
extern print_newline
extern find_word
extern read_string
extern print_err

global _start

_start:
	%ifdef ENABLE_LOOP
		%define LOOP_OR_EXIT .loop
	%else
		%define LOOP_OR_EXIT .end
	%endif

	sub rsp, BYTES_TO_READ
	.loop:
		mov rdi, rsp
		mov rsi, BYTES_TO_READ
		call read_string
		cmp rax, BYTES_TO_READ-1
		ja .too_long
		cmp rax, 0
		je .end
		mov rdi, rsp
		mov rsi, dict_start
		call find_word
		cmp rax, 0
		je .no_match

		mov rdi, rax
		add rdi, 8
		push rdi
		call string_length
		pop rdi
		inc rax
		add rdi, rax

		call print_string
		call print_newline
		mov rdi, EXIT_OK
		jmp LOOP_OR_EXIT
	.too_long:
		mov rdi, err_too_long
		call print_err
		mov rdi, EXIT_ERR
		jmp LOOP_OR_EXIT
	.no_match:
		mov rdi, err_no_match
		call print_err
		mov rdi, EXIT_ERR
		jmp LOOP_OR_EXIT
	.end:
		call exit
